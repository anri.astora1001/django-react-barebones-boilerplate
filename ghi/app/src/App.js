import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import StartMenu from './start/StartMenu';

function App() {
    return (
      <>
      <BrowserRouter>
          <Routes>
            <Route path="/" element={<StartMenu />} />
          </Routes>
      </BrowserRouter>
      <startMenu />
      </>
    );
}

export default App;
